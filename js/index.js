


function getPosts(json) {

  var numberOfPosts=0;

  $.ajax({
    url: "http://api.tumblr.com/v2/blog/"+json[0].tumblr_user+".tumblr.com/posts?api_key="+json[0].api_key,
    dataType: 'jsonp',
    async: false,
    success: function(posts){

      numberOfPosts = Math.min(Math.max(0, posts.response.total_posts), max_number_of_posts);//posts.response.total_posts;

      loadAllContent(numberOfPosts, json);
    }
  });
  contentImageURL = new Array(numberOfPosts);
  contentVideoURL = new Array(numberOfPosts);

  return numberOfPosts;
}


function loadNext20Posts(offSet, numberOfPosts, json) {
  console.log(json[0]);
  $.ajax({
    url: "http://api.tumblr.com/v2/blog/"+json[0].tumblr_user+".tumblr.com/posts?api_key="+json[0].api_key+"&offset="+offSet,
    dataType: 'jsonp',
    success: function(posts){
      var postings = posts.response.posts;


      for (var i in postings) {
        var p = postings[i];



        if(p.type=="photo"){
          var path = p.photos[0].original_size.url;
          var format = path.substring(path.length-3,path.length);

          if(format != "gif"){
          //console.log(format);
          contentImageURL.push({
            src: p.photos[0].original_size.url
          });
        }
        }else if(p.type=="video"){
/*
        contentImageURL.push({
          video:{
            loop: true,
            mute: true,
            src: p.video_url
          }
        });
        */
      }

      numberOfImagesLoaded++;

      //wait to load all posts
      if(numberOfImagesLoaded == numberOfPosts-1){


        $("#example, body").vegas({
          delay: json[0].delay,
          slidesToKeep: 1,
          transition: 'fade2',
          transitionDuration: json[0].transition_duration,
          animation: 'random',
          animationDuration: json[0].animation_duration,
          timer: json[0].timer,
          shuffle: json[0].shuffle,

          slides: contentImageURL
        });
      }
    }
  }
});

}


function loadAllContent(numberOfPosts, json){
  var numberOfImagesPerCall = 20;
  var numberOfCalls = Math.ceil(numberOfPosts/numberOfImagesPerCall)

  //console.log(numberOfCalls);

  for(var i = 0; i < numberOfCalls; i++){
    loadNext20Posts(numberOfImagesPerCall*i, numberOfPosts,json);
  }
}


function loadJSON(callback) {

   var xobj = new XMLHttpRequest();
       xobj.overrideMimeType("application/json");
   xobj.open('GET', 'config.json', true); // Replace 'my_data' with the path to your file
   xobj.onreadystatechange = function () {
         if (xobj.readyState == 4 && xobj.status == "200") {
           // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
          callback(JSON.parse(xobj.responseText));

         }
   };
   xobj.send(null);
}


//_________________________________________________
var numberOfImagesLoaded=0;
var contentImageURL;
var contentVideoURL;
var max_number_of_posts = 1000;

loadJSON(getPosts);
