# Repository for Tumblr Slideshow - EO2

![picture](https://bytebucket.org/onformative/tumblr-slideshow-for-eo2/raw/f1989527e6a30d09abb42bfc7714341c82c17abd/EO2_SS.gif)

### Description

The EO2 Tumblr Slideshow is a small side project developed by onformative to use with the [EO2 display](https://www.electricobjects.com/meet-eo2). It creates a slideshow based on a Tumblr feed. 

Feel free to use, modify or adapt it. It's made in a way to be easy configurable. It can be used to display your own feed or other artist that you like, see "Configure".
As a wise man once said "it is what it is!" it works great for the purpose that is has, show Tumblr pictures in a slideshow. Please note that we won't provide any support or customisation, use at your own risk. 

### How does it work ?

It consists of a very simple html/javascript website that uses: 

* [Tumblr API](https://www.tumblr.com/docs/en/api/v2) - Tumblr queries   
* [Vegas2](www.vegas.jaysalvat.com) - for slideshow

### Configure

* The first thing you will need to do is create a Tumblr OAuth key [here](https://www.tumblr.com/oauth/apps).

* Open the config.json file
* Insert the OAuth key you've just created in "api_key"
* Insert the the username of the Tumblr feed you want to display
* You can also change the slideshow parameters and timings as you wish


```sh
[
{
"api_key": "YOUR_OAuthKey_HERE",
"tumblr_user": "TUMBLR_USERNAME_TO_DISPLAY",
"delay": 30000,
"transition_duration": 4000,
"animation_duration": 50000,
"timer": true,
"shuffle": true
}
]
```

### How to use it ?

After setting up the configuration described before:

* Upload the files to your server, please note that it won't work locally
* Set Your EO2 to load your url https://www.electricobjects.com/set_url
